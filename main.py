import os
from datetime import datetime
import subprocess
from blessings import Terminal

clearCmd = ""
term = Terminal()

if os.name == "nt":  # if Windows
    clearCmd = "cls"
    subprocess.call("if not exist username.txt echo.>username.txt")
    subprocess.call("if not exist last_login.txt echo.>last_login.txt")
    subprocess.call("if not exist contacts.txt echo.>contacts.txt")
    subprocess.call(
        "if not exist contact_conversations\\ mkdir contact_conversations")
else:  # if Mac or Linux
    clearCmd = "clear"
    # bash command to check if "username.txt" exists in current dir
    commandOne = "bash -c \"if [[ ! -f \"username.txt\" ]]; then touch username.txt; fi\""
    # bash command to check if "last_login.txt" exists in current dir
    commandTwo = "bash -c \"if [[ ! -f \"last_login.txt\" ]]; then touch last_login.txt; fi\""
    # bash command to check if "contacts.txt" exists in current dir
    commandThree = "bash -c \"if [[ ! -f \"contacts.txt\" ]]; then touch contacts.txt; fi\""
    os.system(commandOne)  # run commandOne
    os.system(commandTwo)  # run commandTwo
    os.system(commandThree)  # run commandThree
    commandFour = "bash -c \"if [[ ! -d \"contact_conversations\" ]]; then mkdir contact_conversations; fi\""
    os.system(commandFour)  # run commandFour


content = ""
usernameFile = open("username.txt", "r+")
content = usernameFile.readlines()
try:
    username = content[0]
except:
    username = ""
contacts = []
chosenContact = ""
doClear = False


def username_set(usrname=""):  # sets username
    global username
    global usernameFile
    global content

    lineCount = len(content)

    if usrname == "None":
        usernameFile.close()
        usernameFile = open("username.txt", "w")
        usernameFile.write("")
        usernameFile.close()
        return

    if lineCount > 1:
        print("Error: too many lines in \"username.txt\", please clear you have in there.")
        os._exit(1)
    elif lineCount == 0:
        usernameFile.close()
        usernameFile = open("username.txt", "w")
        username = input("Please enter your username > ")
        usernameFile.write(username)
        usernameFile.close()
    elif lineCount == 1:
        username = content[0]


def set_last_login():  # sets the last login date/time and prints it
    os.system(clearCmd)
    content = ""
    loginFile = open("last_login.txt", "r+")
    content = loginFile.readlines()
    lineCount = len(content)
    dateTime = datetime.now().strftime("%d/%m/%Y %H:%M")

    if lineCount > 1:
        print("Error: too many lines in \"last_login.txt\", please clear whatever you have in there.")
        os._exit(1)
    elif lineCount == 0:
        loginFile.close()
        loginFile = open("last_login.txt", "w")
        loginFile.write(dateTime)
        print("Welcome! Please set your username below")
    elif lineCount == 1 and username != "":
        lastLoginMessage = "Last login: {0} | Logged in at: {1}".format(
            content[0], datetime.now().strftime("%d/%m/%y %H:%M"))
        print("Welcome back {0}!{1}{2}".format(username, term.move_x(
            term.width-len(lastLoginMessage)), lastLoginMessage))
        loginFile.close()
        loginFile = open("last_login.txt", "w")
        loginFile.write(dateTime)
    elif lineCount == 1 and username == "":
        loginFile.close()
        loginFile = open("last_login.txt", "w")
        loginFile.write(dateTime)
        print("Welcome! Please set your username below")


def show_contact_menu():  # show contact menu
    global contacts
    global chosenContact
    global username
    global doClear

    moveCmd = ""

    if doClear == True:
        os.system(clearCmd)
        doClear = False

    while True:
        contacts.sort()
        print("\n{0}Contact list:".format(moveCmd))
        if len(contacts) > 0:
            for x in range(0, len(contacts)):
                print("\t({0}) {1}".format(x+1, contacts[x]))
        print("\t(+) Add a contact")
        print("\t(-) Remove a contact")
        print("\t(l) Log out")
        print("\t(q) Quit program")
        choice = input("Please enter a choice > ")
        if choice == "+":
            newContact = ""
            while newContact == "":
                newContact = input("\nPlease enter name of new contact > ")
                if newContact == "":
                    print("Error: name cannot be empty")
                else:
                    newContact = list(newContact)
                    newContact[0] = newContact[0].upper()
                    newContact = ''.join(newContact)
            contacts.append(newContact)
            write_contacts_file()
            continue
        elif choice == "-":
            canExit = False
            while not canExit:
                print("\nContacts available for deletion: ")
                for x in range(0, len(contacts)):
                    print("\t({0}) {1}".format(x+1, contacts[x]))
                choice = input("Please enter a choice > ")
                if choice.isdigit():
                    choice = int(choice)
                    if choice > 0 and choice <= len(contacts):
                        contacts[choice-1:choice] = []
                        write_contacts_file()
                        canExit = True
                    elif choice < 1:
                        print("Error: entered value is too small")
                        continue
                    elif choice >= len(contacts)+1:
                        print("Error: entered value is too large")
                    else:
                        continue
                else:
                    print("Error: entered value is not a number\n")
                continue
        elif choice.isdigit():
            choice = int(choice)
            if choice > 0 and choice <= len(contacts):
                start_conversation(choice)
            else:
                print("Error: a contact with that numerical designation does not exist")
        elif choice == "l":
            print("\nUser '{0}' logged out".format(username))
            username_set(usrname="None")
            os.system(clearCmd)
            os._exit(0)
        elif choice == "q":
            os._exit(0)
        elif choice == "":
            print("Error: value can't be empty")
            continue
        else:
            print("Error: not a recognised value")


def write_contacts_file():  # writes the contacts array to a file
    global contacts

    contactsFile = open("contacts.txt", "w+")
    for x in range(0, len(contacts)):
        contactsFile.write("{0}\n".format(contacts[x]))
    contactsFile.close()


def read_contacts_file():  # reads the contacts from the file and appends them into an array
    global contacts

    contactsFile = open("contacts.txt", "r")
    for line in enumerate(contactsFile):
        contacts.append(line[1].replace("\n", ""))


# prints the conversation header everytime <ENTER> is pressed at the top of the terminal/console
def print_conversation_header(contactNum):
    global contacts

    height = term.height
    width = term.width

    quitMessage = "Enter \":quit\" (or \":q\") to quit"

    print("{0}".format(term.move(0, 0)) + " "*width, end='')
    selectedContact = contacts[contactNum-1]
    print("{0}Now talking to: '{1}'...{2}{3}\n".format(term.move(0, 0),
                                                       selectedContact, term.move_x(width-len(quitMessage)), quitMessage))


def start_conversation(contactNum):  # starts a conversation with the chosen contact
    global contacts
    global doClear

    height = term.height
    width = term.width
    message = ""

    os.system(clearCmd)

    selectedContact = contacts[contactNum-1]
    quitMessage = "Enter \":quit\" (or \":q\") to quit"

    print("Now talking to: '{0}'...{1}{2}\n".format(
        selectedContact, term.move_x(width-len(quitMessage)), quitMessage))
    print("+--------------------------+")
    print("|                    Hey!  |")
    print("+--------------------------+")
    print()

    while True:
        for x in range(0, 4):
            if x == 0:
                with term.location(0, height):
                    message = input(" > ")
                    print_conversation_header(contactNum)
            else:
                with term.location(0, height-x):
                    print(" "*width, end='')
        bufferWidth = width-len(message)-20
        while bufferWidth <= 75:
            bufferWidth = bufferWidth + 1

        if bufferWidth == 76 or bufferWidth == 77:
            message = message[:len(message)-bufferWidth] + "\n" + \
                term.move_x(bufferWidth) + message[len(message)-bufferWidth:]
        if len(message) != 0:
            if message == ":quit" or message == ":q":
                os.system(clearCmd)
                doClear = True
                return
            print("Buffer width: {0}, char count: {1}".format(
                bufferWidth, len(message)))
            print("{0}{1}".format(term.move_x(bufferWidth), message))


def main():
    read_contacts_file()
    set_last_login()
    username_set()
    show_contact_menu()


main()
